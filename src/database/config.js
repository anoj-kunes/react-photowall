import * as firebase from 'firebase'

var config ={
    apiKey: "AIzaSyCi8S3H-0tEhj5IlzJjxo-rPBnrZ1Uq7z0",
    authDomain: "photowall-adc9d.firebaseapp.com",
    databaseURL: "https://photowall-adc9d.firebaseio.com",
    projectId: "photowall-adc9d",
    storageBucket: "photowall-adc9d.appspot.com",
    messagingSenderId: "1013088521260",
    appId: "1:1013088521260:web:8bb6f670ddd3bcc3fd5589",
    measurementId: "G-HX7B06QMQB"
  };

  firebase.initializeApp(config);

  const database = firebase.database()

  export { database }

