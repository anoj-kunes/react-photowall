import React, {Component} from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

// function Photo(props) {
//     const post = props.post
//         return <figure className="figure">
//             <img className="photo" src={post.imageLink} alt={post.description}></img>
//             <figcaption><p> {post.description} </p></figcaption>
//             <div className="button-container"><button className="remove-button"> remove </button></div>
//         </figure>
//   }

class Photo extends Component {
    render() {
        const post = this.props.post
        return <figure className="figure">
            <Link to={ `/single/${post.id}` }>
            <img className="photo" src={post.imageLink} alt={post.description}></img>
            </Link>
            <figcaption><p> {post.description} </p></figcaption>
            <div className="button-container"><button className="remove-button" onClick = {() => {
                this.props.startRemovingPost(this.props.index, post.id)
                this.props.history.push('/');
            }}> remove </button>
            <Link className="button" to={ `/single/${post.id}` }>
                <div className="comment-count">
                    <div className="speech-bubble">
                    
                    </div>         
                    {this.props.comments[post.id] ? this.props.comments[post.id].length : 0}   
                </div>
            </Link>
            </div>
        </figure>
    }
}

Photo.propTypes = {
    post: propTypes.object.isRequired
};

export default Photo;