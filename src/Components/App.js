import Main from './Main'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { removePost } from '../redux/actions'
import { withRouter } from 'react-router'
import * as actions from '../redux/actions'

// bining all the state so it can be accessed by props
function mapStateToProps(state) {
    return {
        posts: state.posts,
        comments: state.comments
    }
}
// binding all the actions so it can be accessed by props
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

const App = withRouter(connect(mapStateToProps, mapDispatchToProps)(Main))

export default App;