import React, {Component} from 'react';
import Title from './Title'
import stylesheet from '../styles/stylesheet.css'
import Photowall from './PhotoWall'
import AddPhoto from './AddPhoto'
import {Route, Link} from 'react-router-dom'
import Single from './Single'

import {removePost} from '../redux/actions';

class Main extends Component {

  constructor() {
    super();
  }

  state = {
    loading: true
  }

  componentDidMount() {
    this.props.startLoadingPost().then(() => this.setState({loading: false}));
    this.props.startLoadingComments();
  }

    render() {
      console.log(this.props);
      return (<div>
        <h1>
          <Link to="/">PhotoWall</Link>
        </h1>
        <Route exact path = "/" render={() => (
          <div>
          <Photowall {...this.props} />
          </div>
        )} />

      <Route exact path = "/AddPhoto" render={({history}) => (
        <AddPhoto {...this.props} />
      )} />

      <Route path="/single/:id" render={(params) => (
        <Single loading={this.state.loading} {...this.props} {...params}/>
      )} >

      </Route>
      </div>)
    }
  }

  export default Main;
