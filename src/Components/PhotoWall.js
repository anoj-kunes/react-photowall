import React, {Component} from 'react';
import Photo from './Photo';
import propTypes from 'prop-types';
import {Link} from 'react-router-dom';

// function PhotoWall(props) {
//     return <div className="photoGrid">
//             {props.posts.map((post, i) => <Photo key={i} post={post}/> )}
//         </div>
// }

class PhotoWall extends Component {

    render() {
        
        return <div>
            <Link to="/AddPhoto" className="addIcon"></Link>
            <div className="photoGrid">
            {this.props.posts
            .sort((x, y) =>  y.id - x.id )
            .map((post, i) => <Photo key={post.id} post={post} {...this.props} index={i}/> )}
        </div>
        </div>
    }
}

PhotoWall.propTypes = {
    posts: propTypes.array.isRequired
};


export default PhotoWall;